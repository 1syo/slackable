yammer = new require('yammer').Yammer
YAMMER_API_TOKEN = process.env.YAMMER_API_TOKEN || ""
DB = require './db'

class YammerMessage
  constructor: ->
    @yam = new yammer access_token: YAMMER_API_TOKEN

  recent: (callback) ->
    DB.get (err, message_id) =>
      @yam.messagesReceived {qs: {newer_than: message_id}}, (res, json) -> callback(json)

module.exports = YammerMessage
