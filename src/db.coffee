REDISCLOUD_URL = process.env.REDISCLOUD_URL || ""
url = require("url")
redis = require("redis")
_ = require('underscore')

class DB
  @max_id: (messages) ->
    (_.max messages, (message) -> message.id()).id()

  @set: (messages) ->
    client = DB.conn()
    client.set("yammer_message_id", DB.max_id(messages), redis.print)
    client.quit()

  @get: (callback) ->
    client = DB.conn()
    client.get "yammer_message_id" , (err, message_id) =>
      callback(err, message_id)
    client.quit()

  @conn: ->
    if REDISCLOUD_URL
      config = url.parse(REDISCLOUD_URL)
      client = redis.createClient(config.port, config.hostname)
      client.auth(config.auth.split(":")[1])
    else
      client = redis.createClient()
    client

module.exports = DB
