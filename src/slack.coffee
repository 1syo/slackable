request = require('request')
_ = require('underscore')

SLACK_USER_ID = process.env.SLACK_USER_ID || 0
SLACK_API_TOKEN = process.env.SLACK_API_TOKEN || ""
YAMMER_USER_ID = process.env.YAMMER_USER_ID || 0

class Slack
  @create_list: (json) ->
    _.map json.messages, (message) => new Slack(message, json.references)

  constructor: (@message, @references) ->

  channel = ->
    SLACK_USER_ID

  sender_id = ->
    @message.sender_id

  sender_name = ->
    users = @references.filter (row) =>
      if row.type == "user" && row.id == sender_id.call(@)
        true
      else
        false
    users[0].full_name

  text = ->
    @message.body.plain

  privacy = ->
    @message.privacy

  url = ->
    @message.web_url

  created_at = ->
    date = new Date(@message.created_at)
    date.toLocaleString();

  color = ->
    if privacy.call(@) == "public"
      "#4183c4"
    else
      ""

  payload = ->
    token: SLACK_API_TOKEN
    channel: channel.call(@)
    parse: "full"
    attachments: JSON.stringify([
      {
        pretext: "[Yammer] \##{this.id()} New #{privacy.call(@)} message from #{sender_name.call(@)}"
        text: text.call(@)
        color: color.call(@)
        fields: [
          {title: "URL", value: url.call(@)},
          {title: "From", value: sender_name.call(@), short: true}
          {title: "Created at", value: created_at.call(@), short: true}
        ]
      }
    ])

  sendable = ->
    sender_id.call(@) != Number(YAMMER_USER_ID)

  id: ->
    @message.id

  send: ->
    # console.log this.id()
    return unless sendable.call(@)

    options = {
      uri: "https://slack.com/api/chat.postMessage"
      form: payload.call(@)
    }

    request.post options, (err, res, body) -> console.log body

module.exports = Slack
